//test

$(document).ready(function () {

	// 载入数据
	chrome.storage.sync.get(['using', 'restrictUrlList'], function(data) {
		// 保存历史
		if (data.using) {
			$('#using').attr("checked", true);
		} else {
			$('#using').attr("checked", false);
		}
		
		// 临时查询数据
		var showCount = 0;
		for (var i in data.restrictUrlList) {
			var url = data.restrictUrlList[i];
	
			++showCount;

			if (showCount % 2 == 0) {
				$('#restrictUrl tbody').append('<tr><th scope="row">' + showCount + '</th><td class="linkWord"><a href="' + url + '">' + url + '</a></td><td class="queryword' + showCount + '">' + '-' + '</td></tr>');
			} else {
				$('#restrictUrl tbody').append('<tr class="active"><th scope="row">' + showCount + '</th><td class="linkWord"><a href="' + url + '">' + url + '</a></td><td class="queryword' + showCount + '">' + '-' + '</td></tr>');
			}
		}
	});
	
	// 启用和禁用
	$('#using').click(function() {
		var using = $('#using').is(':checked');
		chrome.storage.sync.set({'using': using}, function(){
			console.log('set storage');
		});
	});
	
	// 启用和禁用
	$('#clearUrl').click(function() {
		chrome.storage.sync.set({'restrictUrlList': new Array()}, function(){
			console.log('set storage');
		});
		
		$('#restrictUrl tbody').empty();
	});

});
